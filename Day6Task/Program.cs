﻿// See https://aka.ms/new-console-template for more information
using Day6Task.Model;
using Day6Task.Repository;

//Contact contact = new Contact();


ContactRepository contactrepository = new ContactRepository();
List<Contact> contactList = contactrepository.GetAllDetails();
foreach (Contact item in contactList)
    Console.WriteLine(item);


Console.WriteLine(" ContactDetails List After addition");
Contact contact = new Contact() { Name = "Abhishek", Address = "UP", City = "Lucknow", Phone = "98989898989" };
contactrepository.AddContact(contact);
foreach (Contact item in contactList)
{
    Console.WriteLine(item);
}

Console.WriteLine("Delete Operation");
Console.WriteLine("Enter contact to be deleted");
var itemDeleted = Console.ReadLine();
Console.WriteLine(contactrepository.DeleteContactWithName(itemDeleted));
foreach (Contact item in contactList)
{
    Console.WriteLine(item);
}

Console.WriteLine("After Updating.....");
Console.WriteLine("Enter contact to Update...");
var contactUpdate = Console.ReadLine();
contactrepository.UpdateContactByName(contactUpdate);
Console.WriteLine("After Updating.....");
foreach (Contact item in contactList)
{
    Console.WriteLine(item);
}


