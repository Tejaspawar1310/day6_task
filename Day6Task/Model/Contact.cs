﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6Task.Model
{
    internal class Contact
    {
        public string Name { get; set; }    
        public string Address { get; set; }
        
        public string City { get; set; }
      
        public string Phone { get; internal set; }

        public override string ToString()
        {
            return $"Name: {Name}\t Address: {Address}\t City: {City}\t Phone:{Phone}";
        }
    }
}
